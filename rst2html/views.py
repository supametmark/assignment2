from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.files.storage import default_storage
from django.contrib import auth
from django.contrib.auth.models import User
import os


def login_page(request):
    return render(request, 'rst2html/login.html')

def register_page(request):
    return render(request, 'rst2html/register.html')
def home_page(request):
    if request.user.username == "" :
        return render(request,'rst2html/login.html')
    else:
        return render(request,'rst2html/home_page.html')
def register_compilers(request):
    try:
        if request.POST['button'] == 'Submit':
            if request.method == 'POST':
                username = request.POST['username']
                password = request.POST['password']
                confirm_password = request.POST['confirm-password']
                e_mail = request.POST['e-mail']
                first_name = request.POST['first_name']
                last_name = request.POST['last_name']
                if username == '' or password == '' or confirm_password == '' or e_mail == '':
                    message = "Your data is not complete !!"
                    return render(request, 'rst2html/register.html', {'message': message})
                else:
                    if password == confirm_password:
                        user_list = User.objects.all()
                        for user in user_list:
                            if username == user.username:
                                message = 'Username is was already !!'
                                return render(request, "rst2html/register.html", {'message': message})
                        new_user = User.objects.create_user(username, e_mail, password)
                        new_user.is_staff = False
                        new_user.first_name = first_name
                        new_user.last_name = last_name
                        
                        new_user.save()
                    else:
                        message = "Passwords do not match !!"
                        return render(request, "rst2html/register.html", {'message': message})
            return redirect('/login.html')
        else:
            return redirect('/login.html')
    except:
        return redirect('/register.html')


def login_compilers(request):
    try:
        if request.POST['button'] == 'Login':
            if request.user.username != '':
                return HttpResponseRedirect('/')
            elif request.method == 'POST':
                username = request.POST['username']
                password = request.POST['password']
                user = auth.authenticate(username=username, password=password)
                if None != user:
                    auth.login(request, user)
                    return HttpResponseRedirect('/')
                else:
                    if username == '' and password == '':
                        return redirect('rst2html/login.html')
                    else:
                        message = 'Username not found !!'
                        return render(request, 'rst2html/login.html', {'message': message})
        else:
            return redirect('/register.html')
        return redirect('/')
    except:
        return redirect('/login.html')


def logout_compilers(request):
    if request.method == 'POST':
        auth.logout(request)
    return redirect('/login.html')


def rst2html(request):
    rst_name = request.GET['rstname']  
    textinput = open('rst2html/static/rst2html/{}.rst'.format(rst_name), 'w')
    textinput.write(request.GET['convert'])
    textinput.close()
    
    theme = request.GET['theme']  
    os.system('rst2html5 {} rst2html/static/rst2html/{}.rst > rst2html/templates/rst2html/{}.html'.format(theme,rst_name,rst_name))  
    htmlFile = open('rst2html/templates/rst2html/{}.html'.format(rst_name), 'r')
    html_content = ""
    for item in htmlFile:
        html_content = html_content + item  
    htmlFile.close()
    os.remove('rst2html/static/rst2html/{}.rst'.format(rst_name))  
    os.remove('rst2html/templates/rst2html/{}.html'.format(rst_name))  
    return HttpResponse(html_content)

def uploadrst(request):
    dirPath = 'rst2html/static/rst2html/'
    if request.FILES != {}:  
        rstFile = request.FILES['rstFile']
        saveFilePath = default_storage.save(dirPath,rstFile)
        os.rename(saveFilePath,'{}textinput.rst'.format(dirPath))
    if request.POST['theme']== "deck":   
        os.system('rst2html5 --deck-js --pretty-print-code --embed-content rst2html/static/rst2html/textinput.rst > rst2html/templates/rst2html/{}.html'.format(rstFile))
        return render(request,'rst2html/{}.html'.format(rstFile))
    elif request.POST['theme']== "clean":
        os.system('rst2html5 rst2html/static/rst2html/textinput.rst > rst2html/templates/rst2html/{}.html'.format(rstFile))    
        return render(request,'rst2html/{}.html'.format(rstFile))
    elif request.POST['theme']== "reveal":
        os.system('rst2html5 --jquery --reveal-js --pretty-print-code rst2html/static/rst2html/textinput.rst > rst2html/templates/rst2html/{}.html'.format(rstFile))    
        return render(request,'rst2html/{}.html'.format(rstFile))
    elif request.POST['theme']== "bootstrap":
        os.system('rst2html5 --bootstrap-css --pretty-print-code --jquery --embed-content rst2html/static/rst2html/textinput.rst > rst2html/templates/rst2html/{}.html'.format(rstFile))    
        return render(request,'rst2html/{}.html'.format(rstFile))
    return HttpResponseRedirect('/')

def uploadpage(request):
    request.session['imageName'] = ''  # clear image's name
    return render(request,'rst2html/uploadpage.html')

def uploadimage(request):
    file = request.FILES['pic']
    dirPath = 'rst2html/static/rst2html/images/'
    saveFilePath = default_storage.save(dirPath, file)
    os.rename(saveFilePath, '{}{}'.format(dirPath, file.name))
    request.session['imageName'] = file.name
    return HttpResponseRedirect(reverse('rst2html:imagecode'))

def imagecode(request):
    return render(request,'rst2html/imagecode.html')






